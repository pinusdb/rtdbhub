# RealTimeDataBase

#### 仓库介绍
整理收集国内实时数据库产品及相关信息。

#### 声明
本仓库仅收集并整理国内实时数据库的产品及相关信息，并不对产品宣传的真实性进行验证（包括但不限于性能、功能、应用场景等）。
1.  若您不希望贵公司的产品出现在本仓库中，请使用贵公司的企业邮箱发送邮件到zhangqhn@foxmail.com 说明贵公司的产品名称。
2.  本仓库所有引用的文档均会说明出处并附上链接，若您认为某些文档侵犯了您的权益请提issue或发送邮件到zhangqhn@foxmail.com我们会尽快处理。

#### 实时数据库的介绍
实时数据库（RTDB-Real Time DataBase) 是数据库管理系统发展的一个分支，它适用于处理不断更新的快速变化的数据及具有时间限制的事务处理。
实时数据库技术是实时系统和数据库技术相结合的产物。实时数据库最起初是基于先进控制和优化控制而出现的，
对数据的实时性要求比较高，因而实时、高效、稳定是实时数据库关键的指标。（引自：我们为什么需要实时数据库 https://www.modb.pro/db/63542）

#### 产品列表

1.  GoldenRTDB  
    庚顿实时数据库，北京庚顿数据科技有限公司， http://www.golden-data.com.cn
2.  openPlant  
    上海麦杰科技股份有限公司， http://www.magustek.com
3.  RealHistorian  
    紫金桥跨平台实时数据库，大庆紫金桥软件技术有限公司，https://www.realinfo.cc/
4.  pSpace  
    力控企业级实时历史数据库，北京力控元通科技有限公司，http://www.sunwayland.com/
5.  KingHistorian  
    亚控工业实时历史数据库平台，北京亚控科技发展有限公司，https://www.kingview.com/pro_info.php?num=1000004
6.  SyncBase  
    SyncBASE大型实时数据库，南京科远智慧科技集团股份有限公司 ，https://www.sciyon.com/index.php/product/info/41.html 
7.  ProcessDB  
    上海数全软件有限公司，https://www.processdb.cn
8.  Vicdas  
    Vicdas工业级实时历史数据库，http://www.vicdas.com
    